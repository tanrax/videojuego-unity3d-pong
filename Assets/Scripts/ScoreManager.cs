﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

	public GameObject miMarc, miBola;
	private bool bGrande = true;

	void OnTriggerEnter2D(Collider2D colInfo) {
		if (colInfo.name == "bola") {
			switch(gameObject.name) {
				case "ParedDerecha":
					GameManager.IMARJUG2++;
					miMarc.GetComponent<Text>().text = GameManager.IMARJUG2.ToString();
					break;
				case "ParedIzquierda":
					GameManager.IMARJUG1++;
					miMarc.GetComponent<Text>().text = GameManager.IMARJUG1.ToString();
					break;
			}
			miBola.GetComponent<BolaScript>().reiniciar();
		}
	}
}
