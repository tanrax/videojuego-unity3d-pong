﻿using UnityEngine;
using System.Collections;

public class CuadriculaScript : MonoBehaviour {

	public Camera miCamara;
	public BoxCollider2D paredSuperior, paredInferior, paredDerecha, paredIzquierda;
	public Transform jugador1, jugador2, flechaJugador1Derecha, flechaJugador1Izquierda, flechaJugador2Derecha, flechaJugador2Izquierda;
	public RectTransform marcadorJug1, marcadorJug2, infoJug1, infoJug2, botonJug1, botonJug2;
	public float posicionJugadores, separacionMarcadoresArriba, margenMarcadores, separacionInfo, separacionBotonesJug, separacionFlechasBorde;
	private Vector3 miPosJug1, miPosJug2, miPosMarJug1, miPosMarJug2, miPosInfoJug1, miPosInfoJug2, miPosBotonStartJug1, miPosBotonStartJug2, miPosFlechaJug1Der, miPosFlechaJug1Izq, miPosFlechaJug2Der, miPosFlechaJug2Izq;

	void Start () {
		// Pared superior
		paredSuperior.size = new Vector2 (miCamara.ScreenToWorldPoint (new Vector3 (Screen.width * 2f, 0f, 0f)).x, 1f);
		paredSuperior.transform.position  = new Vector2 (0f, miCamara.ScreenToWorldPoint (new Vector3 (0f, Screen.height, 0f)).y + 0.5f);
	
		// Pared inferior
		paredInferior.size = new Vector2 (miCamara.ScreenToWorldPoint (new Vector3 (Screen.width * 2f, 0f, 0f)).x, 1f);
		paredInferior.transform.position  = new Vector2 (0f, miCamara.ScreenToWorldPoint (new Vector3 (0f, 0f, 0f)).y - 0.5f);

		// Pared derecha
		paredDerecha.size = new Vector2 (1f, miCamara.ScreenToWorldPoint (new Vector3 (0f, Screen.height * 2f, 0f)).y);
		paredDerecha.transform.position  = new Vector2 ( miCamara.ScreenToWorldPoint (new Vector3 (Screen.width, 0f, 0f)).x + 0.5f, 0f);

		// Pared izquierda
		paredIzquierda.size = new Vector2 (1f, miCamara.ScreenToWorldPoint (new Vector3 (0f, Screen.height * 2f, 0f)).y);
		paredIzquierda.transform.position  = new Vector2 ( miCamara.ScreenToWorldPoint (new Vector3 (0f, 0f, 0f)).x - 0.5f, 0f);

		// Jugador 1
		miPosJug1 = jugador1.transform.position;
		miPosJug1.x = miCamara.ScreenToWorldPoint (new Vector3 (Screen.width + posicionJugadores * -1f, 0f, 0f)).x;
		jugador1.transform.position = miPosJug1;

		// Jugador 2
		miPosJug2 = jugador2.transform.position;
		miPosJug2.x = miCamara.ScreenToWorldPoint (new Vector3 (posicionJugadores, 0f, 0f)).x;
		jugador2.transform.position = miPosJug2;

		// Marcador Jugador 1
		miPosMarJug1 = marcadorJug1.transform.position;
		miPosMarJug1.x = miCamara.ScreenToWorldPoint (new Vector3 (Screen.width / 2 + marcadorJug1.rect.size.y + margenMarcadores, 0f, 0f)).x;
		miPosMarJug1.y = miCamara.ScreenToWorldPoint (new Vector3 (0f, Screen.height - separacionMarcadoresArriba, 0f)).y;
		marcadorJug1.transform.position = miPosMarJug1;
		
		// Marcador Jugador 2
		miPosMarJug2 = marcadorJug2.transform.position;
		miPosMarJug2.x = miCamara.ScreenToWorldPoint (new Vector3 (Screen.width / 2 - marcadorJug2.rect.size.y - margenMarcadores, 0f, 0f)).x;
		miPosMarJug2.y = miCamara.ScreenToWorldPoint (new Vector3 (0f, separacionMarcadoresArriba, 0f)).y;
		marcadorJug2.transform.position = miPosMarJug2;

		// Info Jugador 1
		miPosInfoJug1 = infoJug1.transform.position;
		miPosInfoJug1.x = miCamara.ScreenToWorldPoint (new Vector3 (Screen.width / 2 + separacionInfo, 0f, 0f)).x;
		miPosInfoJug1.y = miCamara.ScreenToWorldPoint (new Vector3 (0f, Screen.height / 2, 0f)).y;
		infoJug1.transform.position = miPosInfoJug1;

		// Info Jugador 2
		miPosInfoJug2 = infoJug2.transform.position;
		miPosInfoJug2.x = miCamara.ScreenToWorldPoint (new Vector3 (Screen.width / 2 - separacionInfo, 0f, 0f)).x;
		miPosInfoJug2.y = miCamara.ScreenToWorldPoint (new Vector3 (0f, Screen.height / 2, 0f)).y;
		infoJug2.transform.position = miPosInfoJug2;

		// Boton Jugador 1
		miPosBotonStartJug1 = botonJug1.transform.position;
		miPosBotonStartJug1.x = miCamara.ScreenToWorldPoint (new Vector3 (Screen.width / 2 + separacionInfo + separacionBotonesJug, 0f, 0f)).x;
		miPosBotonStartJug1.y = miCamara.ScreenToWorldPoint (new Vector3 (0f, Screen.height / 2, 0f)).y;
		botonJug1.transform.position = miPosBotonStartJug1;
		
		// Boton Jugador 2
		miPosBotonStartJug2 = botonJug2.transform.position;
		miPosBotonStartJug2.x = miCamara.ScreenToWorldPoint (new Vector3 (Screen.width / 2 - separacionInfo - separacionBotonesJug, 0f, 0f)).x;
		miPosBotonStartJug2.y = miCamara.ScreenToWorldPoint (new Vector3 (0f, Screen.height / 2, 0f)).y;
		botonJug2.transform.position = miPosBotonStartJug2;

		// Flecha Jugador 1 derecha
		miPosFlechaJug1Der = flechaJugador1Derecha.transform.position;
		miPosFlechaJug1Der.x = miCamara.ScreenToWorldPoint (new Vector3 (Screen.width - separacionFlechasBorde, 0f, 0f)).x;
		miPosFlechaJug1Der.y = miCamara.ScreenToWorldPoint (new Vector3 (0f, Screen.height - separacionFlechasBorde, 0f)).y;
		flechaJugador1Derecha.transform.position = miPosFlechaJug1Der;

		// Flecha Jugador 1 izquierda
		miPosFlechaJug1Izq = flechaJugador1Izquierda.transform.position;
		miPosFlechaJug1Izq.x = miCamara.ScreenToWorldPoint (new Vector3 (Screen.width - separacionFlechasBorde, 0f, 0f)).x;
		miPosFlechaJug1Izq.y = miCamara.ScreenToWorldPoint (new Vector3 (0f, separacionFlechasBorde, 0f)).y;
		flechaJugador1Izquierda.transform.position = miPosFlechaJug1Izq;

		// Flecha Jugador 2 derecha
		miPosFlechaJug2Der = flechaJugador2Derecha.transform.position;
		miPosFlechaJug2Der.x = miCamara.ScreenToWorldPoint (new Vector3 (separacionFlechasBorde, 0f, 0f)).x;
		miPosFlechaJug2Der.y = miCamara.ScreenToWorldPoint (new Vector3 (0f, separacionFlechasBorde, 0f)).y;
		flechaJugador2Derecha.transform.position = miPosFlechaJug2Der;
		
		// Flecha Jugador 2 izquierda
		miPosFlechaJug2Izq = flechaJugador2Izquierda.transform.position;
		miPosFlechaJug2Izq.x = miCamara.ScreenToWorldPoint (new Vector3 (separacionFlechasBorde, 0f, 0f)).x;
		miPosFlechaJug2Izq.y = miCamara.ScreenToWorldPoint (new Vector3 (0f, Screen.height - separacionFlechasBorde, 0f)).y;
		flechaJugador2Izquierda.transform.position = miPosFlechaJug2Izq;
	}
}