﻿using UnityEngine;
using System.Collections;

public class BolaScript : MonoBehaviour {

	public float velocidad, incrementoRebote, limite, tiempoEsperaInicio;

	void Start () {
		reiniciar ();
	}

	void OnCollisionEnter2D(Collision2D colInfo) {
		// Aumenta la velocidad de la pelota cuando rebota
		float iVelX = GetComponent<Rigidbody2D> ().velocity.x;

		if(limite > iVelX) {
			if (colInfo.collider.tag == "Jugador") {
				if(iVelX > 0) {
					iVelX += incrementoRebote;
				} else {
					iVelX -= incrementoRebote;
				}
				GetComponent<Rigidbody2D>().AddForce(new Vector2(iVelX, GetComponent<Rigidbody2D>().velocity.y));
			} else {
				GetComponent<AudioSource>().Play();
			}
		}

		// Otorga efecto de rebote dependiendo de donde rebote
		GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, colInfo.collider.transform.position.y * -30));
	}

	int getLado() {
		int iLado = 1;
		if (Random.Range (1, 3) == 1) {
			iLado = -1;
		}
		return iLado;
	}

	void iniciarFuerza() {
		float fAngulo = Random.Range (10f, velocidad);
		GetComponent<Rigidbody2D> ().AddForce (new Vector2 (velocidad * getLado (), fAngulo * getLado ()));
	}

	public void reiniciar() {
		StartCoroutine (IEreiniciar());
	}

	IEnumerator IEreiniciar() {
		// Quita la fuerza
		GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
		// Reinicia la posicion
		gameObject.transform.position = new Vector2(0, 0);
		yield return new WaitForSeconds (tiempoEsperaInicio);
		iniciarFuerza ();
	}
}
