﻿using UnityEngine;
using System.Collections;

public class SoundJugScript : MonoBehaviour {

	void OnCollisionEnter2D(Collision2D colInfo) {
		if (colInfo.collider.tag == "Bola") {
			GetComponent<AudioSource> ().Play ();
		}
	}
}
