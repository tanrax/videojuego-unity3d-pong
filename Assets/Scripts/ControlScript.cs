﻿using UnityEngine;
using System.Collections;

public class ControlScript : MonoBehaviour {
	
	public KeyCode moverArriba, moverAbajo;
	public float fVelocidad;
	private Vector2 miPos;

	void Update () {
		
		// Eventos
		if (Input.GetKey(moverArriba)) {
			miPos = gameObject.transform.position;
			miPos.y += fVelocidad;
			gameObject.transform.position = miPos;
		} else if (Input.GetKey(moverAbajo)) {
			miPos = gameObject.transform.position;
			miPos.y -= fVelocidad;
			gameObject.transform.position = miPos;
		}
	}
}